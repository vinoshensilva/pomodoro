import { 
    useEffect, 
    useRef,
    useState 
} from "react";

// Import constants
import { DEFAULT_WORK_TIME } from '@/constants/pomodoro';

export default function useTimer(startTime?: number) {
    
    const currentTime = useRef(startTime || DEFAULT_WORK_TIME);
    const [time,setTime] = useState(currentTime.current);
    const intervalID: { current: NodeJS.Timeout | null } = useRef(null);

    useEffect(()=>{
        return ()=>{
            clearTimer();
        }
    },[])

    // Used within the hook only
    function clearTimer(){
        if(intervalID.current){
            clearInterval(intervalID.current);
        }
    }

    function resetTimer(){
        clearTimer();
        currentTime.current = startTime || DEFAULT_WORK_TIME;
        setTime(currentTime.current);
    }

    function pauseTimer(){
        clearTimer();
    }

    const onInterval = () => {
        currentTime.current = currentTime.current - 1;

        setTime(currentTime.current);

        if(currentTime.current <= 0){
            clearTimer();
        }
    }

    function startTimer(){
        
        resetTimer();

        intervalID.current = setInterval(onInterval,1000);
    }


    return { 
        resetTimer, 
        startTimer,
        pauseTimer,
        time
    }
}