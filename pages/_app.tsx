// Import styles
import '../styles/globals.css'

// Import libraries
import { Provider } from "react-redux";
import Head from 'next/head'

// Import types
import type { AppProps } from 'next/app'

// Import store
import { store } from "../store";

function MyApp({ Component, pageProps }: AppProps) {
  return <>
    <Head>
      <title>Pomodoro App</title>
      <meta 
        name="viewport"
        content="initial-scale=1.0, width=device-width" 
      />
    </Head>
    <Provider store={store}>
      <Component {...pageProps} />
    </Provider>
  </> 
}

export default MyApp
