// Import types
import type { NextPage } from 'next'

// Import components
import TasksContainer from "@/components/organisms/tasks-container";
import TimerContainer from '@/components/organisms/timer-container';
import TimerDetailsContainer from '@/components/organisms/timer-details-container';
import { useDispatch } from 'react-redux';
import { useEffect } from 'react';

// Import functions
import { loadTasks } from '@/functions/storage';

// Import actions
import { setTasks } from '@/slices/tasks';

const Home: NextPage = () => {

  // Violation of single responsibility principle
  const dispatch = useDispatch();
  
  useEffect(()=>{
    const tasks = loadTasks();
    dispatch(setTasks({tasks}));
  },[])

  return (
    <div className="font-OpenSans bg-secondary h-screen w-full p-0 py-5 gap-4 md:p-5 grid grid-flow-row content-start sm:justify-center justify-items-stretch" >
      <TimerContainer />
      <TimerDetailsContainer />
      <TasksContainer />
    </div>
  );
}

export default Home