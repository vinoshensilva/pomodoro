// Import libraries
import PropTypes, {InferProps} from 'prop-types';

function TimerDetails(
  {
    finishTime,
    totalPomodoros,
    currentPomodoros
  }: 
  InferProps<typeof TimerDetails.propTypes>
) {
  return (
    <div className='bg-dark flex flex-wrap gap-2 justify-center text-light p-2 rounded-md'>
      <div>
        Total <span className='font-bold'>{totalPomodoros}</span>
      </div>
      <div>
        Finished <span className='font-bold'>{currentPomodoros}</span>
      </div>
      <div>
        Finishes at <span className='font-bold'>{finishTime}</span>
      </div>
    </div>
  )
}

TimerDetails.propTypes = {
  finishTime: PropTypes.string.isRequired,
  totalPomodoros: PropTypes.number.isRequired,
  currentPomodoros: PropTypes.number.isRequired,
}

export default TimerDetails