// Import libraries
import React from 'react'
import { useSelector } from 'react-redux';

// Import components
import TimerDetails from '@/components/organisms/timer-details';

// Import types
import { TStore } from '../../../store';
import { TTask } from '@/types/task';

// Import constants
import { DEFAULT_WORK_TIME } from '@/constants/pomodoro';

function TimerDetailsContainer() {
    
  const tasks = useSelector((state: TStore) => state.tasksReducer.tasks);

  const totalPomodoros = tasks.reduce((total,task : TTask)=>{return total + task.pomodoros},0);
  
  const currentPomodoros = tasks.reduce((total,task : TTask)=>{return total + task.currentPomodoro},0);

  const date = new Date();
  date.setSeconds(date.getSeconds() + (totalPomodoros * DEFAULT_WORK_TIME));

  return (
    <TimerDetails 
      finishTime={date.toLocaleTimeString([], {
        hourCycle: 'h23',
        hour: '2-digit',
        minute: '2-digit'
      })}
      totalPomodoros={totalPomodoros}
      currentPomodoros={currentPomodoros}
    />
  );
}

export default TimerDetailsContainer