// Import libraries
import React from 'react'
import PropTypes, {InferProps} from 'prop-types';
import StyledButton from '@/components/molecules/styled-button';

function Timer({
    time,
    onClickStart,
    onClickPause,
    onClickReset
}:
  InferProps<typeof Timer.propTypes>
) {
  return (
    <div className='mx-auto grid grid-flow-row gap-2'>
      <div className='text-center text-4xl text-light font-medium'>
        {time}
      </div>
      <div className='flex gap-2'>
        <StyledButton
          onClick={onClickStart}
          className="button--primary"
        >
          Start
        </StyledButton>
        <StyledButton
          onClick={onClickPause}
          className="button--primary"
        >
          Pause
        </StyledButton>
        <StyledButton
          onClick={onClickReset}
          className="button--primary"
        >
          Reset
        </StyledButton>
      </div>
    </div>
  )
}

Timer.propTypes = {
    time: PropTypes.string.isRequired,
    onClickStart: PropTypes.func.isRequired,
    onClickPause: PropTypes.func.isRequired,
    onClickReset: PropTypes.func.isRequired,
}

export default Timer