// Import libraries
import React from 'react'
import { useSelector } from 'react-redux'

// Import types
import { TStore } from '../../../store';
import { TTask } from '@/types/task';

// Import components
import TaskContainer from '@/components/molecules/task-container';
import AddTask from '@/components/molecules/add-task';

function TasksContainer() {
  const tasks = useSelector((state: TStore) => state.tasksReducer.tasks);

  function renderTasks () {
    return tasks.map((task: TTask)=>(
      <TaskContainer
        description={task.description}
        currentPomodoro={task.currentPomodoro}
        pomodoros={task.pomodoros}
        id={task.id}
        key={task.id}
      />
    ))
  }

  return (
    <>
      <div className='w-full sm:max-w-lg sm:w-80 mx-auto'>
        <AddTask />

        <div className='grid gap-2'>
          {renderTasks()}
        </div>
      </div>
    </>
  )
}

export default TasksContainer