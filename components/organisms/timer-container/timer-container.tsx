// import components
import Timer from '@/components/organisms/timer'

// Import hooks
import useTimer from '@/hooks/useTimer'
import { useEffect, useRef } from 'react';
import { useDispatch,useSelector } from 'react-redux';

// Import actions
import { incrementPomodoro } from '@/slices/tasks'

// Import types
import { TStore } from '../../../store';

function TimerContainer() {

  const dispatch = useDispatch();

  const tasks = useSelector((state: TStore) => state.tasksReducer.tasks);

  const audioClip = useRef<HTMLAudioElement | null>(null);

  const {
      time,
      startTimer,
      pauseTimer,
      resetTimer
  } = useTimer();

  useEffect(()=>{
    audioClip.current = new Audio('/sounds/rising-pops.mp3')
  },[]);

  useEffect(()=>{
    if(time === 0){
      dispatch(incrementPomodoro());

      if(audioClip.current){
        audioClip.current.play();
      }
    }
  },[time]);

  const onClickStart = () => {
    startTimer();
  }

  const onClickPause = () => {
    pauseTimer();
  }

  const onClickReset = () => {
    resetTimer();
  }

  const minutes = Math.floor(time/60);
  const seconds = (time - minutes * 60).toString().padStart(2,'0');
  const timeString = `${minutes}:${seconds}`;

  return (
    <>
      {
        tasks.length > 0 && 
        <Timer 
          onClickPause={onClickPause}
          onClickReset={onClickReset}
          onClickStart={onClickStart}
          time={timeString}
        />
      }
    </>
  );
}

export default TimerContainer