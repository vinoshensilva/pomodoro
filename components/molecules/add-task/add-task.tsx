// Import components
import React from 'react'
import { useDispatch } from 'react-redux'

// Import components
import StyledButton from '@/components/molecules/styled-button';

// Import icons
import { AddIcon } from '@/components/atoms/icons';

// Import actions
import { addTask } from '@/slices/tasks'

function AddTask() {
  const dispatch = useDispatch();

  function onClickAddTask(){
    dispatch(addTask())
  }

  return (
    <div className='text-center'>
      <StyledButton 
        onClick={onClickAddTask}
        className="button--tertiary"
      >
        <AddIcon 
          className="w-6 h-6"
        />
      </StyledButton>
    </div>
  )
}

export default AddTask