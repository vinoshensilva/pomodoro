// Import libraries
import PropTypes, {InferProps} from 'prop-types';
import { useDispatch } from 'react-redux';

// Import components
import Task from '@/components/molecules/task'
import DeleteTask from '@/components/molecules/delete-task';

// Import actions
import { updateTaskDescription,updateTaskPomodoros } from '@/slices/tasks';

function TaskContainer(
  {
      id,
      description,
      currentPomodoro,
      pomodoros
  }: 
  InferProps<typeof TaskContainer.propTypes>
) {

  const dispatch = useDispatch();

  function onChangeDescription(newDescription: string){
    dispatch(updateTaskDescription({id,description: newDescription}));
  }

  function onChangePomodoros(newPomodoros: number){
    dispatch(updateTaskPomodoros({id,pomodoros: newPomodoros}))
  }

  return (
    <div
      className={`grid gap-2 ${ currentPomodoro !== pomodoros ? ' bg-white hover:bg-light ' : ' bg-red-200'} p-2 sm:rounded-lg`}
    >
      <DeleteTask
        id={id}
      />
      <Task
        description={description}
        currentPomodoro={currentPomodoro}
        pomodoros={pomodoros}
        onChangeDescription={onChangeDescription}
        onChangePomodoros={onChangePomodoros}
      />
    </div>
  )
}

TaskContainer.propTypes = {
    currentPomodoro: Task.propTypes.currentPomodoro,
    description: Task.propTypes.description,
    pomodoros: Task.propTypes.pomodoros,
    id: PropTypes.string.isRequired,
}

export default TaskContainer