// Import libraries
import React from 'react';
import PropTypes, {InferProps} from 'prop-types';
import { useDispatch } from 'react-redux';

// Import component
import StyledButton from '@/components/molecules/styled-button';

// Import icons
import { TrashIcon } from '@/components/atoms/icons';

// Import actions
import { removeTask } from '@/slices/tasks';

function DeleteTask(
    { id }: 
    InferProps<typeof DeleteTask.propTypes>
) {

  const dispatch = useDispatch();
  
  function onClickTaskDelete(){
    dispatch(removeTask({ id }));
  }

  return (
    <div className='flex justify-end w-full'>
        <StyledButton 
            onClick={onClickTaskDelete} 
            className="button--secondary"
        >
            <TrashIcon 
                className="w-4 h-4"
            />
        </StyledButton>
    </div>
  );
}

DeleteTask.propTypes = {
    id: PropTypes.string.isRequired
}

export default DeleteTask