// Import libraries
import React from 'react'
import PropTypes, { InferProps } from 'prop-types';

// Import components
import StyledInput from '@/components/molecules/styled-input';

function Task(
  {
    description,
    currentPomodoro,
    pomodoros,
    onChangeDescription,
    onChangePomodoros
  }: 
  InferProps<typeof Task.propTypes>
) {
  
  return (
    <>
      <div className='w-full flex justify-between items-center gap-4 p-1'>
          <div className='flex-grow'>
            <StyledInput
              value={description}
              className="input--primary"
              onChange={(e : React.ChangeEvent<HTMLInputElement>)=>{onChangeDescription(e.target.value)}}
            />
          </div>
          <div className='text-dark flex'>
            <input 
                value={currentPomodoro}
                className="input--primary w-4"
                readOnly
              />
            <input 
              type='number'
              value={pomodoros}
              className="input--primary w-5 overflow-hidden"
              onChange={(e : React.ChangeEvent<HTMLInputElement>)=>{onChangePomodoros(parseInt(e.target.value))}}
            />
          </div>
      </div>
    </>
  )
}

Task.propTypes = {
  description: PropTypes.string.isRequired,
  currentPomodoro: PropTypes.number.isRequired,
  pomodoros: PropTypes.number.isRequired,
  onChangeDescription: PropTypes.func.isRequired,
  onChangePomodoros: PropTypes.func.isRequired,
}

export default Task;