// import components
import Input from '@/components/atoms/input';

// Import function
import { getStyle } from '@/functions/component';

const CLASSES = [
    'input--primary',
];

function StyledInput({
    value,
    onChange,
    className,
    ...restProps
}) {
  const style = getStyle(CLASSES,className);

  return (
    <Input
        onChange={onChange}
        className={style}
        value={value}
        {...restProps}
    />
  )
}

export default StyledInput;