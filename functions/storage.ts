import { TTask } from "@/types/task";

const tasksKey = 'tasks';

export const saveTasks = function (tasks: TTask[]) {
    try {
        const saveData = JSON.stringify(tasks);
        localStorage.setItem(tasksKey,saveData);
    } catch (error) {
        console.error(error);        
    }
}

export const loadTasks = function()  : TTask[] 
{
    try {
        const data = localStorage.getItem(tasksKey);
        
        return data ? JSON.parse(data) : [];
    } catch (error) {
        console.error(error);
                
        return [];
    }
}