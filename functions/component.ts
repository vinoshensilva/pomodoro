export const getStyle = (classes : Array<string>, className : string) => {
  if (!classes || classes.length === 0) return '';

  if (!className) return classes[0];

  const style = classes.some((substring: string) => className.includes(substring)) ? className : `${classes[0]} ${className}`;

  return style;
};