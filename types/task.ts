export type TTask = {
    description: string;
    pomodoros: number;
    id: string, //Used for keys when rendering the tasks a list
    currentPomodoro: number
};