// Import libraries
import {      
    createListenerMiddleware,
    isAnyOf,
    Action,
    ListenerEffectAPI,
} from '@reduxjs/toolkit'

import {
    addTask,
    incrementPomodoro,
    removeTask,
    updateTaskDescription,
    updateTaskPomodoros
} from '@/slices/tasks'

const listenerMiddleware = createListenerMiddleware();

import { loadTasks, saveTasks } from '@/functions/storage';
import { TStore } from '../store';

listenerMiddleware.startListening({
    matcher: isAnyOf(
        addTask,
        incrementPomodoro,
        removeTask,
        updateTaskDescription,
        updateTaskPomodoros
    ),
    // KIV: not typed here for the listener api
    effect: async(action: Action,listenerApi: any)=>{
        const tasks = listenerApi.getState().tasksReducer.tasks;

        saveTasks(tasks);
    }
})

export const tasksListenerMiddleware = listenerMiddleware;