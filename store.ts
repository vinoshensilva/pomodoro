// Import libraries
import { configureStore } from "@reduxjs/toolkit";
import { tasksListenerMiddleware } from "@/middlewares/redux";

// Import reducers
import reducer from "./reducer/reducer";

// functions
// import { loadTasks } from "@/functions/storage";

export const store = configureStore({ 
    reducer,
    // preloadedState: {
    //     tasksReducer: {
    //         tasks: loadTasks()
    //     }
    // },
    middleware: (getDefaultMiddleware) => getDefaultMiddleware().prepend(tasksListenerMiddleware.middleware),
});
export type TStore = ReturnType<typeof store.getState>;