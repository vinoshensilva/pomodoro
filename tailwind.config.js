module.exports = {
  content: [
    "./pages/**/*.{js,ts,jsx,tsx}",
    "./components/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    borderWidth: {
      0: '0',
      0.5: '0.5px',
      1: '1px',
      2: '2px',
      3: '3px',
      4: '4px',
      6: '6px',
      8: '8px',
    },
    extend: {
      colors: {
        primary: '#DD0426',
        secondary: '#F02D3A',
        dark: '#273043',
        gray: '#9197AE',
        light: '#EFF6EE',
        turquoise: '#40E0D0',
        'cadet-blue': '#5F9EA0'
      },
    },
    fontFamily: {
      OpenSans: ['Open Sans'],
    },
  },
  plugins: [],
}