import { 
    createSlice, 
    PayloadAction 
} from '@reduxjs/toolkit';

// Import types
import { TTask } from 'types/task';

// Define task states
export interface tasksState {
    tasks: TTask[];
}

// Define payload types
type TRemoveTaskPayload = {
    id: string
}

type TUpdateDescriptionPayload = {
    id: string,
    description: string
}

type TUpdatePomodorosPayload = {
    id: string,
    pomodoros: number
}

type TSetTasksPayload = {
    tasks: TTask[],
}

const initialState : tasksState = {
    tasks: [],
};

const tasksSlice = createSlice({
    name: 'tasks',
    initialState,
    reducers: {
        addTask: (state : tasksState) => {
            state.tasks.push({
                description: 'Task description',
                currentPomodoro: 0,
                pomodoros: 1,
                id: `${Math.floor(Math.random() * 10)}${Date.now()}`
            });
        },
        removeTask: (state : tasksState, { payload }: PayloadAction<TRemoveTaskPayload>)=>{
            const { id } = payload;

            const removeIndex = state.tasks.findIndex((value)=> value.id === id);

            state.tasks.splice(removeIndex,1);
        },
        updateTaskDescription: (state : tasksState, { payload }: PayloadAction<TUpdateDescriptionPayload>)=>{
            const { description,id } = payload;

            const taskIndex = state.tasks.findIndex((value)=> value.id === id);

            state.tasks[taskIndex].description = description;
        },
        updateTaskPomodoros: (state : tasksState, { payload }: PayloadAction<TUpdatePomodorosPayload>)=>{
            const { pomodoros,id } = payload;

            const taskIndex = state.tasks.findIndex((value)=> value.id === id);
    
            state.tasks[taskIndex].pomodoros = pomodoros;
        },
        incrementPomodoro: (state : tasksState) => {
            // Increment the pomodoro of the first incomplete task found
            const taskIndex = state.tasks.findIndex((value)=> value.currentPomodoro < value.pomodoros);

            if(taskIndex !== -1){
                state.tasks[taskIndex].currentPomodoro++;
            }
        },
        setTasks: (state : tasksState, { payload }: PayloadAction<TSetTasksPayload>) => {
            const {tasks} = payload;

            state.tasks = tasks; 
        }
    }
});

export const tasksReducer = tasksSlice.reducer;

export const {
    incrementPomodoro,
    addTask,
    removeTask,
    updateTaskDescription,
    updateTaskPomodoros,
    setTasks
} = tasksSlice.actions;